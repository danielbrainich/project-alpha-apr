from django.shortcuts import render, redirect
from tasks.models import Task
from tasks.forms import CreateTaskForm
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def list_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"my_task_list": tasks}
    return render(request, "tasks/my_tasks.html", context)
