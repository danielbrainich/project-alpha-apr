from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


def redirect_to_project_list(request):
    return redirect("list_projects")


urlpatterns = [
    path("", redirect_to_project_list, name="home"),
    path("accounts/", include("accounts.urls")),
    path("projects/", include("projects.urls")),
    path("tasks/", include("tasks.urls")),
    path("admin/", admin.site.urls),
]
